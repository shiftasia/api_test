var frisby = require('frisby');
var config = require('../api_endpoint.json');
var data = require('../data/data_post.json');
var data_put = require('../data/data_put.json');

console.log("API POST method");
frisby.create("API POST method")
  .post(config.base_url, {
    "content": data.content
  }, {json: true})
  // verify response's status
  .expectStatus(201)
  // verify response's header
  .expectHeaderContains('content-type', 'application/json')
  // verify response's type of body values
  .expectJSONTypes({
    content: String
  })
  // verify response's body values
  .expectJSON({
    content: data.content
  })
  .after(function(err, res, body) {    
    var dynamically_created_blobId = res.headers['x-jsonblob'];
    console.log("API GET method");
    frisby.create("API GET method")
      .get(config.base_url + dynamically_created_blobId)      
      // verify response's status
      .expectStatus(200)
      // verify response's header
      .expectHeaderContains('content-type', 'application/json')
      // verify response's type of body values
      .expectJSONTypes({
        content: String
      })
      // verify response's body values
      .expectJSON({
        content: data.content
      })
      .after(function(err, res, body) {
        console.log("API PUT method");
        frisby.create("API PUT method")
          .put(config.base_url + dynamically_created_blobId, {
            "content":data_put.content
          }, {json: true})
          // verify response's status
          .expectStatus(200)
          // verify response's header
          .expectHeaderContains('content-type', 'application/json')
          // verify response's type of body values
          .expectJSONTypes({
            content: String
          })
          // verify response's body values
          .expectJSON({
            content: data_put.content
          })
          .after(function(err, res, body) {
            console.log("API DELETE method");
            frisby.create("API DELETE method")
              .delete(config.base_url + dynamically_created_blobId)
              .expectStatus(200)
              .toss();
          })
          .toss();
      })
      .toss();
  })
  .toss();

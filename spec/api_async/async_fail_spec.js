var frisby = require('frisby');
var config = require('../api_endpoint.json');
var data_post = require('../data/data_post.json');
var data_get = require('../data/data_get.json');
var data_put = require('../data/data_put.json');

var dynamically_created_blobId = "xxxx-xxxx-xxxx-xxxx";

console.log("API POST method");
frisby.create("API POST method")
  .post(config.base_url, {
    "content": data_post.content
  }, {json: true})
  // verify response's status
  .expectStatus(201)
  // verify response's header
  .expectHeaderContains('content-type', 'application/json')
  // verify response's type of body values
  .expectJSONTypes({
    content: String
  })
  // verify response's body values
  .expectJSON({
    content: data_post.content	
  })
  .after(function(err, res, body) {
	console.log("after POST method");
    dynamically_created_blobId = res.headers['x-jsonblob'];
    //console.log(dynamically_created_blobId);
  })
  .toss();

console.log("API GET method");    
frisby.create("API GET method")
  .get(config.base_url + dynamically_created_blobId)
  // verify response's status
  .expectStatus(200)
  // verify response's header
  .expectHeaderContains('content-type', 'application/json')
  // verify response's type of body values
  .expectJSONTypes({
    content: String
  })
  // verify response's body values
  .expectJSON({
    content: data_get.content
  })
  .after(function(err, res, body) {
	console.log("after GET method");    
  })
  .toss();

console.log("API PUT method");    
frisby.create("API PUT method")
  .put(config.base_url + dynamically_created_blobId, {
    "content":data_put.content
  }, {json: true})
  // verify response's status
  .expectStatus(200)
  // verify response's header
  .expectHeaderContains('content-type', 'application/json')
  // verify response's type of body values
  .expectJSONTypes({
    content: String
  })
  // verify response's body values
  .expectJSON({
    content: data_put.content
  })
  .after(function(err, res, body) {
	console.log("after PUT method");    
  })
  .toss();

console.log("API DELETE method");
frisby.create("API DELETE method")
  .delete(config.base_url + dynamically_created_blobId)
  .expectStatus(200)
  .after(function(err, res, body) {
	console.log("after DELETE method");    
  })
  .toss();
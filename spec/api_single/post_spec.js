var frisby = require('frisby');
var config = require('../api_endpoint.json');
var data = require('../data/data_post.json');

console.log("API POST method")
frisby.create("API POST method")
  .post(config.base_url, {
    "content": data.content
  }, {json: true})
  // verify response's status
  .expectStatus(201)
  // verify response's header
  .expectHeaderContains('content-type', 'application/json')
  // verify response's type of body values
  .expectJSONTypes({
    content: String
  })
  // verify response's body values
  .expectJSON({
    content: data.content
  })
  .toss();

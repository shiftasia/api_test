var frisby = require('frisby');
var config = require('../api_endpoint.json');
var data = require('../data/data_get.json');

console.log("API GET method")
frisby.create("API GET method")
  .get(config.base_url + data.blob_id)
  // verify response's status
  .expectStatus(200)
  // verify response's header
  .expectHeaderContains('content-type', 'application/json')
  // verify response's type of body values
  .expectJSONTypes({
    content: String
  })
  // verify response's body values
  .expectJSON({
    content: data.content
  })
  .toss();

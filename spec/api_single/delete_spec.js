var frisby = require('frisby');
var config = require('../api_endpoint.json');
var data = require('../data/data_delete.json');

console.log("API DELETE method")
frisby.create("API DELETE method")
  .delete(config.base_url + data.blob_id)
  .expectStatus(200)
  .toss();

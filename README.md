# API Test Automation using Frisby and Node.js

## Technologies used:

* Node.js

* Frisby

* Jasmine-node

* Async

## Guide
> **Installation**
> > download and install Node.js from https://nodejs.org
>
> > install Frisby with command line: npm install --save-dev frisby
>
> > install Jasmine-node with command line: npm install -g jasmine-node
>
> **Run API tests from CLI**
> > cd spec
> 
> > run single test, for example:
> jasmine-node api_single/get_spec.js
>
> > run a set of tests, for example:
> jasmine-node api_single/
>
>
## Repository Structure
> **node_modules**
> > contains modules (frisby and async) for run
>
> **spec**
> > api_single: contains single API tests for HTTP methods (POST, GET, PUT, DELETE)
>
> > api_async: contains a single API test that run HTTP methods (POST, GET, PUT, DELETE) asynchronously
>
> > api_sync: contains 2 API tests that run HTTP methods (POST, GET, PUT, DELETE) synchronously with built-in method after() and module async
>
